﻿===========================================================
Internal Akasia code, to test the round trip calls against the http://saas.akasiacloud server
Copyright © Akasia 2021. All rights reserved.
Date : 02 April 2021
Ver#sion: 1.0.0

https://akasiacloud.com/termsandconditions/
No warranty or indemnification.  Use at your own risk.  Subject to change
===========================================================


===========================================================
Prerequisites
===========================================================
A. As we are using the python script in this utility the python is needed with version > 3.5. To check the python version you can check the output of command “python -v“, sometimes you may need to run “python3 -v“. To upgrade the python version refer to the links given in the references section.

B. pip3 the module manager for python3 modules is required in the system. Check if pip3 is configured in the system

C. Curl with version >=7.75.0 is needed in the system. To check the version of curl you can execute "curl -V". For upgrading curl, you can follow the steps mentioned in "Updating Curl to >=7.75.0 version" section.

D. Basic knowledge of JSON files is needed.

E. Machine specifics:
        1. This framework is tested on Linux based OSs like Ubuntu, Fedora, etc.
        2. The steps are given, assuming that apt package manager is configured in the system.

===========================================================
Updating Curl to >=7.75.0 version
===========================================================
Below steps can be followed to update the curl to 7.76.0. These steps are given considering the OS has apt as a package manager. You can also try downloading the binary from https://curl.se/download.html and setup up in your host for other OSs.


A. apt update
B. Check if the curl version is below 7.75.0. If yes then follow along.
        The below is a suggestion for how to build curl-7.76.
        * Please use your discretion if you are already using either an older or later version of curl and use it on your system
        * This is assuming that its ok to either install OR replace the version of curl
C. Remove the current installed curl if installed:
        >> apt remove curl
        >> apt purge curl
D. Install the tools to compile this release and curl dependencies:
        >> apt-get update
        >> apt-get install -y libssl-dev autoconf libtool make
E. Download and install the latest release from https://curl.haxx.se/download/curl-7.76.0.zip. Run below commands one by one in ssh terminal:
        >> cd /usr/local/src
        >> rm -rf curl*
        >> wget https://curl.haxx.se/download/curl-7.76.0.zip
        >> unzip curl-7.76.0.zip
F. Compile
        >> cd curl-7.76.0     # enter the directory where curl was unpacked #
        >> ./buildconf
        >> ./configure --with-ssl 
        >> make
        >> make install
G. Update the system’s binaries
        >> mv /usr/bin/curl /usr/bin/curl.bak
        >> cp /usr/local/bin/curl /usr/bin/curl
H. Validate
        >> curl -V


Please connect if you see any issue upgrading this binary.

===========================================================
Configuration file: Required
===========================================================
A. You can refer below JSON file for the rest of the section:
{
	"variables": [
		{
			"apiHost": "https://saas.akasiacloud.com"
		},
		{
			"username": "akasia-username@domain.com"
		},
		{
			"password": "Password@123"
		},
		{
			"client_email_list": "abc@xyz.com"
		},
		{
			"administrator_email_list": "abc@xyz.com"
		},
		{
			"performance_email_list": "abc@xyz.com"
		},
		{
			"smtp_server":"smtp-server-host"
		},
		{
			"smtp_port":587
		},
		{
			"smtp_username":"smtp-server-username"
		},
		{
			"smtp_password":"smtp-server-password"
		},
		{
			"smtp_from":"sender-email-address"
		},
		{
			"email_subject":"Sample Subject Line"
		}
	]
}

There are below json notations which you can see in the variables section:

        Here you can specify the variables and their values. Which can be referred throughout the test suite execution. In this section, one tuple(key value pair) in the array will represent the variable-name and variable-value. If that variable needs to be referred further the variable_name should be wrapped up with $ and {} i.e. ${variable_name}. There are some variables that are commonly used in JSON configuration such as:

        * apiHost: The API server against which we will be running the tests

        * username, password: The credentials will be used while getting the authentication token. You can check the references of these variables in the rest of the JSON file

        * client_email_list: Comma separated email_ids will be used by the script for sending email.

        * administrator_email_list: Comma separated email_ids will be used by the script for sending email.
        
        * performance_email_list: Comma separated email_ids will be used by the script for sending email.

		* email_subject: Email Subject Line.

        * smtp details such as smtp_server(smtp server IP address), smtp_port(smtp service port number), smtp_username(smtp authentication username), smtp_password(smtp authentication password), smtp_from(smtp email from address)

===========================================================
Running the Healthcheck
===========================================================
* Prepare the json configuration.
        - Setup the login details("variables"=>"username","variables"=>"password") in configuration file
        
        - Setup the comma separated email list in sections "variables"=>"error_email_list","variables"=>"performance_email_list" and "variables"=>"administrator_email_list".

        - Setup the SMTP server details

* Review the Behave Configuration file:
        - Check if "confFile" key is pointing to the configuration file, This configuration file should have all details mentioned in "Configuration file" section

* Run automation utility with the below command:
        Installing all dependencies:
	#Caution:  The below action could change some of the installed python modules and lib
        >> pip3 install -r requirement.txt

        For running the healthcheck:
        >> behave

===========================================================
Monitoring APIs by setting up Cron
===========================================================
A. This folder path should be /akasia/api-monitoring-framework-bdd where all files resides.

B. Follow the steps mentioned in "Using the script" section.

C. The cron is created to be triggered in the interval of 15 minutes for both Production and staging environments. Review and modify api-monitoring.cron file accordingly.

D. If you can notice in this cron file,
        * The root folder is /akasia/api-monitoring-framework-bdd, feel free to change the location.
        * The logs of this utility will be captured in file "/akasia/api-monitoring-framework-bdd/logs.txt". This path can be changed in cron file.


E. Copy the cron file to /etc/cron.d folder, and reload the cron service.

        >> cp api-monitor.cron /etc/cron.d/api-monitor
        >> systemctl restart crond.service


===========================================================
References:
===========================================================
* https://curl.se/docs/manpage.html
* https://tech.serhatteker.com/post/2019-12/upgrade-python38-on-ubuntu/
* https://curl.se/download.html
* https://en.wikipedia.org/wiki/List_of_HTTP_status_codes