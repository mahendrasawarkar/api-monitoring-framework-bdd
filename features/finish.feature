Feature: Finish the HealthCheck Run

Scenario: Send UP email to all if previous API Server Status was DOWN
    When The Previous API Service State is DOWN
    Then Send Email to client_email_list with UP message
    And Send Email to administrator_email_list with UP message
    And Send Email to performance_email_list with UP message
    And Mark the API Service State as UP