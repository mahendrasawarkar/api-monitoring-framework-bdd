Feature: Test the Get Profile API

Scenario: Call Profile API with 2 retries and Get the Profile Details
    Given The configuration variables for Get Profile API are set
    And Curl command for Get Profile API is called - first retry
    When There is problem with Curl command execution
    Then Call Curl command for Get Profile API - second retry

Scenario: Check if there is problem with Curl command execution
    When There is problem with Curl command execution
    Then Mark the feature as FAILED
    And Send Email to client_email_list with DOWN message
    And Send Email to administrator_email_list with DOWN message
    And Mark the API Service State as DOWN
    And Skip rest of the "steps,scenarios,features"

Scenario: Collect the API response from Command Response
    When The Curl command finished gracefully
    Then Collect the API response from Command Response

Scenario: Check if API response is valid
    When The API response is not JSON
    Then Mark the feature as FAILED
    And Send Email to administrator_email_list with DOWN message
    And Send Email to client_email_list with DOWN message
    And Mark the API Service State as DOWN
    And Skip rest of the "steps,scenarios,features"

Scenario: Check if API Server is SLOW
    When The Curl command finished gracefully
    And StatusCode matches with expected
    And Response Body has firstName,lastName,email,tenant_id
    And Actual response time is greater than expected response time
    Then Expose variables firstName,lastName,email,tenant_id
    And Mark the feature as SLOW
    And Send Email to performance_email_list with SLOW message
    And Skip rest of the "steps,scenarios"

Scenario: Check if response status code is invalid
    When The Curl command finished gracefully
    And StatusCode Does not matches with expected
    Then Call Curl command for Get Profile API - second retry

Scenario: Collect the API response from Command Response
    When The Curl command finished gracefully
    Then Collect the API response from Command Response

Scenario: Check if API response is not valid
    When The API response is not JSON
    Then Mark the feature as FAILED
    And Send Email to administrator_email_list with DOWN message
    And Send Email to client_email_list with DOWN message
    And Mark the API Service State as DOWN
    And Skip rest of the "steps,scenarios,features"

Scenario: Expose all variables if all looks good
    When The Curl command finished gracefully
    And StatusCode matches with expected
    And Response Body has firstName,lastName,email,tenant_id
    Then Expose variables firstName,lastName,email,tenant_id
    And Mark the feature as SUCCESSFUL
    And Skip rest of the "steps,scenarios"

Scenario: Check if there is problem with Curl command execution - second retry
    When There is problem with Curl command execution
    Then Mark the feature as FAILED
    And Send Email to client_email_list with DOWN message
    And Send Email to administrator_email_list with DOWN message
    And Mark the API Service State as DOWN
    And Skip rest of the "steps,scenarios,features"

Scenario: Check if response status code is valid in second retry
    When The Curl command finished gracefully
    And StatusCode Does not matches with expected
    Then Mark the feature as FAILED
    And Send Email to client_email_list with DOWN message
    And Send Email to administrator_email_list with DOWN message
    And Mark the API Service State as DOWN
    And Skip rest of the "steps,scenarios,features"

Scenario: Check if API Server is SLOW - second retry
    When The Curl command finished gracefully
    And StatusCode matches with expected
    And Response Body has firstName,lastName,email,tenant_id
    And Actual response time is greater than expected response time
    Then Expose variables authKey
    And Mark the feature as SLOW
    And Send Email to performance_email_list with SLOW message
    And Skip rest of the "steps,scenarios"

Scenario: Check if response body has firstName,lastName,email,tenant_id
    When The Curl command finished gracefully
    And StatusCode matches with expected
    And Response Body has not firstName,lastName,email,tenant_id
    Then Mark the feature as False
    And Send Email to client_email_list with DOWN message
    And Send Email to administrator_email_list with DOWN message
    And Skip rest of the "steps,scenarios,features"

Scenario: Expose all variables if all looks good
    When The Curl command finished gracefully
    And StatusCode matches with expected
    And Response Body has firstName,lastName,email,tenant_id
    Then Expose variables firstName,lastName,email,tenant_id
    And Mark the feature as SUCCESSFUL
    And Skip rest of the "steps,scenarios"
