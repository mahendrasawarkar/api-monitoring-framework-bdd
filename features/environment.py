#===========================================================
# Internal Akasia code, to test the round trip calls against the http://saas.akasiacloud server
# Copyright © Akasia 2021. All rights reserved.
# Date : 02 April 2021
# Ver#sion: 1.0.0

# https://akasiacloud.com/termsandconditions/
# No warranty or indemnification.  Use at your own risk.  Subject to change
#===========================================================
import os
import behave_restful.app as br_app
from datetime import datetime

def before_all(context):
    print("The API Monitor is Triggered with Configuration file :" + str(context.config.userdata.get("confFile")) + " And with Timestamp : " + datetime.now().strftime("%H:%M:%S"))
    context.skip_next_features = []
    context.skip_next_features.append(False)
    api_state = context.config.userdata.get("api_state_file")
    if not os.path.exists(api_state):
        f = open(api_state, "w")
        f.write("UP")
        f.close()

    this_directory = os.path.abspath(os.path.dirname(__file__))
    br_app.BehaveRestfulApp().initialize_context(context, this_directory)
    context.hooks.invoke(br_app.BEFORE_ALL, context)


def after_all(context):
    context.hooks.invoke(br_app.AFTER_ALL, context)


def before_feature(context, feature):
    if hasattr(context, 'skip_next_features'):
        print("The context skip_next_features is " + str(context.skip_next_features[0]))
        if context.skip_next_features[0]:
            feature.skip(False)
    context.hooks.invoke(br_app.BEFORE_FEATURE, context, feature)


def after_feature(context, feature):
    context.hooks.invoke(br_app.AFTER_FEATURE, context, feature)


def before_scenario(context, scenario):
    context.hooks.invoke(br_app.BEFORE_SCENARIO, context, scenario)


def after_scenario(context, scenario):
    context.hooks.invoke(br_app.AFTER_SCENARIO, context, scenario)


def before_step(context, step):
    context.hooks.invoke(br_app.BEFORE_STEP, context, step)


def after_step(context, step):
    context.hooks.invoke(br_app.AFTER_STEP, context, step)


def before_tag(context, tag):
    context.hooks.invoke(br_app.BEFORE_TAG, context, tag)


def after_tag(context, tag):
    context.hooks.invoke(br_app.AFTER_TAG, context, tag)
