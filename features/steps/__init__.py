#===========================================================
# Internal Akasia code, to test the round trip calls against the http://saas.akasiacloud server
# Copyright © Akasia 2021. All rights reserved.
# Date : 02 April 2021
# Ver#sion: 1.0.0

# https://akasiacloud.com/termsandconditions/
# No warranty or indemnification.  Use at your own risk.  Subject to change
#===========================================================

# we need to include behave-restful's step defs
# or they won't be available in .feature files
from behave_restful.lang import *