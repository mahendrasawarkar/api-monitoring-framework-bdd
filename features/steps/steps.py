#===========================================================
# Internal Akasia code, to test the round trip calls against the http://saas.akasiacloud server
# Copyright © Akasia 2021. All rights reserved.
# Date : 02 April 2021
# Ver#sion: 1.0.0

# https://akasiacloud.com/termsandconditions/
# No warranty or indemnification.  Use at your own risk.  Subject to change
#===========================================================
import json
import smtplib
import subprocess
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from behave import *
from prettytable import PrettyTable

global_configuration_variable = {}
global_runtime_variable = {}
overall_test_results = []


class Test:
    def __init__(self, scenario, url, isSuccess):
        self.scenario = scenario
        self.url = url
        self.isSuccess = isSuccess


@given("The configuration variables for Authentication API are set")
def step_impl(context):
    feature_name = context.feature.name
    update_global_runtime_variables(context)
    login_api_details = {"api_url": "${apiHost}/akasia/rest/report/1/auth/login",
                         "headers_array": ["Content-Type:application/json"],
                         "body": {},
                         "additional_append": " -u ${username}:${password} ",
                         "method": "GET",
                         "expected_response_time": 5,
                         "expected_status_code": 200}
    global_configuration_variable[feature_name] = login_api_details


@given("The configuration variables for Get Profile API are set")
def step_impl(context):
    feature_name = context.feature.name
    update_global_runtime_variables(context)
    profile_api_details = {"api_url": "${apiHost}/akasia/rest/report/1/auth/getProfile",
                           "headers_array": ["Authorization:Basic ${authKey}"],
                           "body": {},
                           "additional_append": "",
                           "method": "GET",
                           "expected_response_time": 5,
                           "expected_status_code": 200}
    global_configuration_variable[feature_name] = profile_api_details


@given("The configuration variables for Logout API are set")
def step_impl(context):
    feature_name = context.feature.name
    update_global_runtime_variables(context)
    logout_api_details = {"api_url": "${apiHost}/akasia/rest/report/1/auth/logout",
                          "headers_array": ["Authorization:Basic ${authKey}"],
                          "body": {},
                          "additional_append": "",
                          "method": "GET",
                          "expected_response_time": 1,
                          "expected_status_code": 200}
    global_configuration_variable[feature_name] = logout_api_details


# feature based execute curl command
def call_curl_command(context):
    feature_name = context.feature.name

    # Prepare curl command
    api_url = global_configuration_variable[feature_name]["api_url"]
    headers_array = global_configuration_variable[feature_name]["headers_array"]
    combined_header = ""
    for h in headers_array:
        combined_header = combined_header + " -H \"" + h + "\""
    body = global_configuration_variable[feature_name]["body"]
    body = "--data " + str(body)
    additional_append = global_configuration_variable[feature_name]["additional_append"]
    method = global_configuration_variable[feature_name]["method"]

    curl_command = "curl -s -w ',\"response_header\":{\"time_total\": %{time_total},\"response_code\": %{response_code}}}'" + \
                   " --max-time 120 --request " + method + " \"" + api_url + "\" " + body + " " + combined_header + " " + additional_append
    # replace the placeholders
    curl_command = replace_curl_command_variables(curl_command)
    print("The curl command we are executing is " + curl_command)
    # run curl command
    curl_command_response = subprocess.getstatusoutput(curl_command)
    print("The actual command response is " + str(curl_command_response))
    global_configuration_variable[feature_name]["actual_command_response"] = curl_command_response


@given("Curl command for {api_details} is called {other_details}")
@when("Curl command for {api_details} is called {other_details}")
@then("Curl command for {api_details} is called {other_details}")
@when("Call Curl command {api_details} {other_details}")
@then("Call Curl command {api_details} {other_details}")
def step_impl(context, api_details, other_details):
    call_curl_command(context)


def replace_curl_command_variables(curl_command):
    variableKeyValuePairs = global_runtime_variable.items()
    for key, value in variableKeyValuePairs:
        curl_command = curl_command.replace("${" + key + "}", str(value))
    return curl_command


@when("The TimeOut period is triggered")
def step_impl(context):
    feature_name = context.feature.name
    actual_command_response_code = global_configuration_variable[feature_name]["actual_command_response"][0]
    if actual_command_response_code != 28:
        assert False, 'It was not TimeOut'


@when("The TimeOut period is Not triggered")
def step_impl(context):
    feature_name = context.feature.name
    actual_command_response_code = global_configuration_variable[feature_name]["actual_command_response"][0]
    if actual_command_response_code == 28:
        assert False, 'It was TimeOut'


@when("The Curl command doesn't finished gracefully")
def step_impl(context):
    feature_name = context.feature.name
    actual_command_response_code = global_configuration_variable[feature_name]["actual_command_response"][0]
    if actual_command_response_code != 0:
        assert True, "The Curl command doesn't finished gracefully"
    else:
        assert False


@when("The Curl command finished gracefully")
def step_impl(context):
    feature_name = context.feature.name
    actual_command_response_code = global_configuration_variable[feature_name]["actual_command_response"][0]
    if actual_command_response_code == 0:
        yes(context)
    else:
        no(context)


@then("Collect the API response from Command Response")
def step_impl(context):
    feature_name = context.feature.name
    response = global_configuration_variable[feature_name]["actual_command_response"][1]
    if str(response).startswith(","):
        response = "{\"response_body\":\"\"" + str(response)
    else:
        response = "{\"response_body\":" + str(response)
    global_configuration_variable[feature_name]["final_response"] = response


@when("The API response is not JSON")
def step_impl(context):
    feature_name = context.feature.name
    response = global_configuration_variable[feature_name]["final_response"]
    try:
        response = json.loads(response)
    except ValueError as err:
        yes(context)
        return
    no(context)
    global_configuration_variable[feature_name]["final_response"] = response


@when("There is problem with Curl command execution")
def step_impl(context):
    feature_name = context.feature.name
    print("The Scenarios is " + str(context.scenario))
    # import pdb
    # pdb.set_trace()
    actual_command_response_code = global_configuration_variable[feature_name]["actual_command_response"][0]
    print("The actual_command_response is " + str(global_configuration_variable[feature_name]["actual_command_response"]))
    if actual_command_response_code != 0:
        assert True, "The Curl command doesn't finished gracefully"
    else:
        no(context)
        return


@when("Actual response time is less than expected response time")
def step_impl(context):
    feature_name = context.feature.name
    print("The final object is " + str(global_configuration_variable[feature_name]))
    actualResponseTime = global_configuration_variable[feature_name]["final_response"]["response_header"]["time_total"]
    expectedResponseTime = global_configuration_variable[feature_name]["expected_response_time"]
    if actualResponseTime < expectedResponseTime:
        yes(context)
    else:
        no(context)

@when("Actual response time is greater than expected response time")
def step_impl(context):
    feature_name = context.feature.name
    print("The final object is " + str(global_configuration_variable[feature_name]))
    actualResponseTime = global_configuration_variable[feature_name]["final_response"]["response_header"]["time_total"]
    expectedResponseTime = global_configuration_variable[feature_name]["expected_response_time"]
    if actualResponseTime >= expectedResponseTime:
        yes(context)
    else:
        no(context)

@when("StatusCode Does not matches with expected")
def step_impl(context):
    feature_name = context.feature.name
    actualStatusCode = global_configuration_variable[feature_name]["final_response"]["response_header"]["response_code"]
    expectedResponseCode = global_configuration_variable[feature_name]["expected_status_code"]
    if actualStatusCode != expectedResponseCode:
        yes(context)
    else:
        no(context)


@when("StatusCode matches with expected")
def step_impl(context):
    feature_name = context.feature.name
    # import pdb
    # pdb.set_trace()
    actualStatusCode = global_configuration_variable[feature_name]["final_response"]["response_header"]["response_code"]
    expectedResponseCode = global_configuration_variable[feature_name]["expected_status_code"]
    if actualStatusCode == expectedResponseCode:
        yes(context)
    else:
        no(context)


@when("Response Body has not {keys}")
def step_impl(context, keys):
    feature_name = context.feature.name
    actualResponseBody = global_configuration_variable[feature_name]["final_response"]["response_body"]
    condition = True
    for key in keys.split(","):
        condition = condition & (key in actualResponseBody)
    if condition:
        no(context)
    else:
        yes(context)


@when("Response Body has {keys}")
def step_impl(context, keys):
    feature_name = context.feature.name
    actualResponseBody = global_configuration_variable[feature_name]["final_response"]["response_body"]
    condition = True
    for key in keys.split(","):
        condition = condition & (key in actualResponseBody)
    if condition:
        yes(context)
    else:
        no(context)


@then("Mark the feature as {execution_status}")
def step_impl(context, execution_status):
    feature_name = context.feature.name
    global_configuration_variable[feature_name]["execution_status"] = execution_status
    overall_test_results.append(
        Test(feature_name, global_configuration_variable[feature_name]["api_url"], execution_status))


@then("Execution status of feature is {value}")
def step_impl(context, value):
    feature_name = context.feature.name
    if global_configuration_variable[feature_name]["execution_status"] == value:
        yes(context)
    else:
        no(context)


@then("Expose variables {variables}")
def step_impl(context, variables):
    feature_name = context.feature.name
    actualResponseBody = global_configuration_variable[feature_name]["final_response"]["response_body"]
    for exposeVariables in variables.split(","):
        if exposeVariables in actualResponseBody:
            global_runtime_variable[exposeVariables] = actualResponseBody[exposeVariables]


def update_global_runtime_variables(context):
    conf_file = context.config.userdata.get("confFile")
    with open(conf_file) as data_file:
        data = json.load(data_file)
        for variable in data['variables']:
            variable_json_object = json.dumps(variable)
            key_value_pairs = json.loads(variable_json_object).items()
            for key, value in key_value_pairs:
                global_runtime_variable[key] = value


def yes(context):
    assert True


def no(context):
    context.scenario.skip(False)


@then("Skip rest of the {values}")
def step_impl(context, values):
    if "steps" in values:
        context.scenario.skip(False)
        print("The steps are skipped")
    if "scenarios" in values:
        context.feature.skip(False)
        print("The scenarios are skipped")
    if "features" in values:
        context.skip_next_features[0] = True


@when("The Previous API Service State is {api_server_state}")
def step_impl(context, api_server_state):
    api_state = context.config.userdata.get("api_state_file")
    f = open(api_state, "r")
    previousAPIState = f.read()
    if api_server_state == previousAPIState:
        yes(context)
    else:
        no(context)


@then("Mark the API Service State as {api_server_state}")
def step_impl(context, api_server_state):
    api_state = context.config.userdata.get("api_state_file")
    f = open(api_state, "w")
    f.write(api_server_state)
    f.close()


@then("Send Email to {email_list} with {api_server_state} message")
def step_impl(context, email_list, api_server_state):

    emaiToList = global_runtime_variable[email_list]
    smtpServer = global_runtime_variable["smtp_server"]
    smtpPortNumber = global_runtime_variable["smtp_port"]
    smtpUserName = global_runtime_variable["smtp_username"]
    smtpPassword = global_runtime_variable["smtp_password"]
    smtpFromEmailId = global_runtime_variable["smtp_from"]

    tabular_fields = ["Scenario", "URL", "Test Result"]
    tabular_table = PrettyTable()
    tabular_table.field_names = tabular_fields
    for testResult in overall_test_results:
        tabular_table.add_row([testResult.scenario, testResult.url, testResult.isSuccess])

    my_message = tabular_table.get_html_string()

    html = """\
    <html>
        <head>
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 5px;
                text-align: left;    
            }    
        </style>
        </head>
    <body>
    <p> Hello,<br><br>
    
        AkasiaCloud API HealthCheck is """ + str(api_server_state) + """<br>
  
        Below are the details for reference<br>
        %s
    </p>
    </body>
    </html>
    """ % my_message

    htmlPart = MIMEText(html, 'html')

    htmlMsg = MIMEMultipart('alternative')
    htmlMsg['Subject'] = global_runtime_variable["email_subject"]
    htmlMsg['From'] = smtpFromEmailId
    htmlMsg['To'] = emaiToList
    htmlMsg.attach(htmlPart)

    print("Email addresses are '" + emaiToList + "'")
    if emaiToList.isspace() or len(emaiToList) == 0:
        print(tabular_table)
    else:
        try:
            email = smtplib.SMTP(smtpServer, smtpPortNumber)
            email.starttls()
            email.login(smtpUserName, smtpPassword)
            email.sendmail(smtpFromEmailId, emaiToList.split(","), htmlMsg.as_string())
            email.quit()
        except Exception as exception:
            print("Error: unable to send email with error message : " + str(exception))
    assert True, "We are sending email to " + emaiToList + " About the Health of API Server which is " + api_server_state
